import { calloutAnimation, slideInOutAnimation, fadeInOutAnimation, enterLeaveAnimation } from '../app-animations/animations';
import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

export class slideClass {
  public slideURL : string;
  public slideTitle: string;
  public slideInfo: string;

  constructor(url: string, title, info: string) {
    this.slideURL = url;
    this.slideTitle = title;
    this.slideInfo = info; 
  }
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    slideInOutAnimation,
    calloutAnimation,
    fadeInOutAnimation,
    enterLeaveAnimation
  ]
})




export class HomeComponent implements OnInit {
  
  config: SwiperConfigInterface = {
    slidesPerView: 1,
    spaceBetween: 50,
    centeredSlides: false,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 3,
            spaceBetween: 40,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    }
  };

  
  
  public title = 'Milli-On-Colours';
  public slidesList = new Array<string>(8);
  public slides = new Array<slideClass>(11);

  constructor() { }

  ngOnInit() {


    this.slides[0] = new slideClass("./assets/images/mel-styles/style-3.jpg", "Lisa", 
    `A shiny semi-permanent full head colour, gorgeous rich, chocolate brown.
      This example was finished with a blunt cut.`);
    
    this.slides[1]  = new slideClass("./assets/images/mel-styles/style-9.jpg", "Helen", 
    `A natural bridal hair-up.  Lots of curls, with a bit of back-combing.  
    Plaits wrapping around creating a loose, natural look.`);

    this.slides[2]  = new slideClass("./assets/images/mel-styles/style-7.jpg", "Sophie", 
    `A 3/4 head of foils underneath the parting; great if you don't want the roots!  
    Some copper and caramel shades in this clients hair, finished with a cut & smooth curls.`);

    this.slides[3] = new slideClass("./assets/images/mel-styles/style-4.jpg", "Lisa B", 
    `Back to back slices of bleach and a gorgeous icy toner to create this look!  
    Finished with a trim and wavy blow-dry.`);

    this.slides[4]  = new slideClass("./assets/images/mel-styles/style-8.jpg", "Tilly", 
    `Crazy colours and pastel tones!  This client had pink, lavendar and purple on pre-lightened hair 
    to brighten her hair up!  Finished with some curls.`);

    this.slides[5]  = new slideClass("./assets/images/mel-styles/style-5.jpg", "Debs", 
    `A 1/2 head of foils to freshen up this clients hair.  Blonde and copper foils to 
    add depth, shine and to bighten the hair, finished with a blunt cut.`);
  
    this.slides[6]  = new slideClass("./assets/images/mel-styles/style-10.jpg", "Beck", 
    `A balayage which is right on trend at the moment!  A rich, chocolate brown at the root 
    blending into rose gold on the pre-lightened ends.  Finished with a cut and curls.`);

    this.slides[7] = new slideClass("./assets/images/mel-styles/style-2.jpg", "Charlotte", 
    `A full head foils of ashy colours.  Icy blondes and a cool brown to break it up slightly.  
    Finished off by straightening.`);

    this.slides[8]  = new slideClass("./assets/images/mel-styles/style-6.jpg", "Debs", 
    `Foils of a cool blonde and lots of rich, warm copper.  
    Finished with a cut, adding a fringe and a wavy blow-dry.`);

    this.slides[9]  = new slideClass("./assets/images/mel-styles/style-1.jpg", "Sara", 
    `Foils of ash blondes and chocolate browns done in slices to make them really chunky!  
    Finished with a cut with lots of choppy layers and a fringe.`);

    this.slides[10]  = new slideClass("./assets/images/Logo_Upscaled.png", "Milli-On-Colours", ``);

    this.slidesList[7] = './assets/images/Logo_Upscaled.png';
    this.slidesList[6] = './assets/images/blossom-600.jpg';
    this.slidesList[5] = './assets/images/girl-with-mag-600.jpg';
    this.slidesList[4] = './assets/images/tools-600.jpg';
    this.slidesList[3] = './assets/images/unsplash-violet-hair-600.jpg';
    this.slidesList[2] = './assets/images/style-4.jpg';
    this.slidesList[1] = './assets/images/style-5.jpg';
    this.slidesList[0] = './assets/images/mel-styles/style-1.jpg';
  }

  public executeSelectedChange = event => {
    console.log(event);
  }
}
