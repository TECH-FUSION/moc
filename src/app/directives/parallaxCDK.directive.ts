import { Directive, Input, ElementRef } from '@angular/core';

import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/overlay';

@Directive({
  selector: '[appParallaxCDK]'
})

export class ParallaxCDKDirective {
  el: HTMLElement;
  initialTop = 0;
  // tslint:disable-next-line:no-input-rename
  @Input('ratio') parallaxRatio = 0.2;

  constructor(
    readonly elRef: ElementRef,
    private readonly scroll: ScrollDispatcher
  ) {
    this.el = elRef.nativeElement;
    this.initialTop = this.el.getBoundingClientRect().top;
    this.el.style.position = 'relative';

    this.scroll.scrolled().subscribe(x => this.onScroll(x || undefined));
    this.scroll.ancestorScrolled(elRef).subscribe(() => console.log('ancestor scroll'));
  }

  onScroll(scrollable?: CdkScrollable) {
    if (!scrollable) {
      return;
    }

    const scrolledElem: HTMLElement = scrollable.getElementRef().nativeElement;
    const scrollY = scrolledElem.scrollTop;

    const parallax = this.initialTop + scrollY * this.parallaxRatio;

    this.el.style.top = `${parallax}px`;
  }
}
