import { Component, OnInit } from '@angular/core';
import { calloutAnimation, slideInOutAnimation, fadeInOutAnimation, enterLeaveAnimation } from '../app-animations/animations';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [
    slideInOutAnimation,
    calloutAnimation,
    fadeInOutAnimation,
    enterLeaveAnimation
  ]
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
