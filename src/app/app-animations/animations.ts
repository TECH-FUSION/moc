import {
  animation,
  trigger,
  stagger,
  transition,
  animate,
  style,
  query,
  state
} from '@angular/animations';


export const calloutAnimation = [
  trigger('calloutAnim',
    [
      transition('* => *', [
        query('.callOut', style({ opacity: 0, transform: 'translateX(-40px' })),
        query(
          '.callOut',
          stagger('500ms', [
            animate(
              '800ms 500ms ease-out',
              style({ opacity: 1, transform: 'translateX(0)' })
            )
          ])
        )
      ])
    ])];

export const slideInOutAnimation = [
  trigger('slideInOut', [
    transition(':enter', [
      style({ transform: 'translateY(-100%)' }),
      animate('200ms ease-in', style({ transform: 'translateY(0%)' }))
    ]),
    transition(':leave', [
      animate('200ms ease-in', style({ transform: 'translateY(-100%)' }))
    ])
  ])];

export const fadeInOutAnimation = [
  trigger('fadeInOut', [
    state(
      'void',
      style({
        opacity: 0
      })
    ),
    transition('void <=> *', animate(1000))
  ])];

export const enterLeaveAnimation = [
  trigger('EnterLeave', [
    state('flyIn', style({ transform: 'translateX(0)' })),
    transition(':enter', [
      style({ transform: 'translateX(-100%)' }),
      animate('0ms 1000ms ease-in')
    ]),
    transition(':leave', [
      animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
    ])
  ])];
