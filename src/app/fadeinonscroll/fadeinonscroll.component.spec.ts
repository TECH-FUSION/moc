import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FadeinonscrollComponent } from './fadeinonscroll.component';

describe('FadeinonscrollComponent', () => {
  let component: FadeinonscrollComponent;
  let fixture: ComponentFixture<FadeinonscrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FadeinonscrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FadeinonscrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
