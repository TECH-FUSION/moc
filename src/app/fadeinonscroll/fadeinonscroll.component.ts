import { Component, HostListener, ElementRef } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-fadeinonscroll',
  template: `
    <div [@scrollAnimation]="state">
      <img width="100%"
        src="/assets/images/tools-600.jpg"
      />
    </div>
  `,
  styles: [],
  animations: [
    trigger('scrollAnimation', [
      state(
        'show',
        style({
          opacity: 1,
          transform: 'translateX(0)'
        })
      ),
      state(
        'hide',
        style({
          opacity: 0,
          transform: 'translateX(100%)'
        })
      ),
      transition('show => hide', animate('700ms ease-out')),
      transition('hide => show', animate('700ms ease-in'))
    ])
  ]
})
export class FadeinonscrollComponent {
  state = 'hide';

  constructor(public el: ElementRef) { }

  // Where el is the DOM element you'd like to test for visibility
  isHidden(el) {
    return (el.offsetParent === null)
  };

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const componentPosition = this.el.nativeElement.offsetTop;
    const scrollPosition = window.pageYOffset;
    if (scrollPosition >= componentPosition - 600) {
      this.state = 'show';
    } else {
      this.state = 'hide';
    }
  }
}
