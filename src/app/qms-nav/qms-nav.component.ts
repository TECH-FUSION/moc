import { Component, ViewChild, ChangeDetectorRef, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, tap, filter, withLatestFrom } from 'rxjs/operators';
import { Router, NavigationEnd } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { MediaObserver } from '@angular/flex-layout';



@Component({
  selector: 'app-qms-nav',
  templateUrl: './qms-nav.component.html',
  styleUrls: ['./qms-nav.component.scss']
})
export class QmsNavComponent {
  @ViewChild('drawer', { static: true }) drawer: MatSidenav;

public title = 'Milli-On-Colours';

isHandset$: Observable<boolean> = this.media.asObservable().pipe(
  map(
    () =>
      this.media.isActive('xs') ||
      this.media.isActive('sm') ||
      this.media.isActive('lt-md')
  ),
  tap(() => this.changeDetectorRef.detectChanges()));


  constructor(
    private media: MediaObserver,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  OnInit() {
    this.isHandset$.subscribe(isHandset => console.log(isHandset));
  }

}
